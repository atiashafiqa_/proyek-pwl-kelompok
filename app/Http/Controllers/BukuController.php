<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Book;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //$buku = DB::table('books')->get();
        $buku = Book::all();
        return view('buku.index', ['buku' => $buku]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('buku.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->validate([
            'judul' => 'required',
            'penulis' => 'required',
            'kategori' => 'required'
        ]);

        /*$buku = new Book;
        $buku->judul = $request->judul;
        $buku->pengarang = $request->pengarang;
        $buku->kategori = $request->kategori;

        $buku->save();*/

        Book::create($request->all());
        return redirect('buku')->with('status', 'Data buku berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Book $buku) {
        return view('buku.detail', ['buku' => $buku]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $buku)
    {
        return view('buku.edit', ['buku' => $buku]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Buku $buku) {
        $request->validate([
            'judul' => 'required',
            'penulis' => 'required',
            'kategori' => 'required'
        ]);
        
        Buku::where('id', $buku->id)
            ->update([
                'judul' => $request->judul,
                'penulis' => $request->penulis,
                'kategori' => $request->kategori
            ]);
        
        return redirect('buku')->with('status', 'Data buku berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $buku)
    {
        Book::destroy($buku->id);
        return redirect('buku')->with('status', 'Data buku dihapus');
    }
}
