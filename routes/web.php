<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\BukuController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthController::class, 'showFormLogin'])->name('login');
Route::get('login', [AuthController::class, 'showFormLogin'])->name('login');
Route::post('login', [AuthController::class, 'login']);
Route::get('register', [AuthController::class, 'showFormRegister'])->name('register');
Route::post('register', [AuthController::class, 'register']);

Route::group(['middleware' => 'auth'], function () {
    Route::get('home', [PagesController::class, 'home'])->name('home');
    Route::get('buku', [BukuController::class, 'index'])->name('buku');
    Route::get('buku/create', [BukuController::class, 'create'])->name('buku/create');
    Route::get('buku/{buku}', [BukuController::class, 'show'])->name('buku/{buku}');
    Route::post('buku', [BukuController::class, 'store']);
    Route::delete('buku/{buku}', [BukuController::class, 'destroy']);
    Route::get('buku/{buku}/edit', [BukuController::class, 'edit'])->name('buku/{buku}/edit');
    Route::patch('buku/{buku}', [BukuController::class, 'update']);
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
});


