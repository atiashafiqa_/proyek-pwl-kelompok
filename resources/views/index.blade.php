@extends('layout/main')

@section('title', 'Sistem Informasi Perpustakaan Rimba')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-15">
            <figure class="text-center">
                <blockquote class="blockquote">
                    <p>A book is a gift you can open again and again.</p>
                </blockquote>
                <figcaption class="blockquote-footer">
                    Garrison Keillor
                </figcaption>
            </figure>
        </div>
    </div>
</div>
@endsection
