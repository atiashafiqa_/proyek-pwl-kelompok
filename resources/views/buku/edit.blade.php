@extends('layout/main')

@section('title', 'Ubah Data Buku')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-10">
            <h3 class="mt-3">Form Ubah Data Buku</h3>
            <form method="POST" action="buku/{{ $buku->id }}">
            @method('patch')
            @csrf 
            <div class="mb-3">
                <label for="judul" class="form-label">Judul</label>
                <input type="text" class="form-control @error('judul') is-invalid @enderror" id="judul" placeholder="Masukkan judul" name="judul" value="{{ $buku->judul }}">
                @error('judul')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="penulis" class="form-label">Penulis</label>
                <input type="text" class="form-control @error('penulis') is-invalid @enderror" id="penulis" placeholder="Masukkan nama penulis" name="penulis" value="{{ $buku->penulis }}">
                @error('penulis')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror            
            </div>
            <div class="mb-3">
                <label for="kategori" class="form-label">Kategori</label>
                <input type="text" class="form-control @error('kategori') is-invalid @enderror" id="kategori" placeholder="Masukkan kategori buku" name="kategori" value="{{ $buku->kategori }}">
                @error('kategori')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            </form>
            <button type="submit" class="mt-3 btn btn-success">Ubah</button>
            @if (session('status'))
                <div class="alert alert-success">{{ session('status') }}</div>
            @endif
        </div>
    </div>
</div>
@endsection
