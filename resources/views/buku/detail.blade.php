@extends('layout/main')

@section('title', 'Detail Buku')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-10">
            <h3 class="mt-3">Detail Buku</h3>
            <table class="table table-hover">
            <thead>
                <tr>
                <th scope="col">ID</th>
                <th scope="col">Judul</th>
                <th scope="col">Penulis</th>
                <th scope="col">Kategori</th>
                <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <td> {{ $buku->id }}</td>
                <td> {{ $buku->judul }}</td>
                <td> {{ $buku->penulis }}</td>
                <td> {{ $buku->kategori }}</td>
                <td>
                    <a href="{{ $buku->id }}/edit" class="mt-3 btn btn-success">Edit</a>
                    <form action="{{ $buku->id }}" method="post" class="d-inline">
                        @method('delete')
                        @csrf
                        <button type="submit" class="mt-3 btn btn-danger">Hapus</button>
                    </form>
                </td>
                </tr>
            </table>
            <a href="/buku" class="mt-3 btn btn-outline-dark">Kembali</a>
        </div>
    </div>
</div>
@endsection
