@extends('layout/main')

@section('title', 'Daftar Buku')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-7">
            <h3 class="mt-3">Daftar Buku Perpustakaan</h3>
            <table class="table table-hover">
            <thead>
                <tr>
                <th scope="col">No.</th>
                <th scope="col">Judul</th>
                <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ( $buku as $book )
                <tr>
                <th scope="row">{{ $loop->iteration }}</th>
                <td> {{ $book->judul }}</td>
                <td>
                    <a href="/buku/{{ $book->id }}" class="badge bg-primary rounded-pill">detail</a>
                </td>
                </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>
    <a href="/buku/create" class="mt-3 btn btn-outline-primary">Tambah Buku</a>
</div>
@endsection
